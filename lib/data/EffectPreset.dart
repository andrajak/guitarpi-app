import 'package:bc_ui_flutter/model/EffectPresetModel.dart';

// for Preset page purpose
var allEffectWithPresets = <EffectPresetModel>[
  EffectPresetModel('Echo', [
  ]),
  EffectPresetModel('Delay', [
  ]),
  EffectPresetModel('Distortion', [
  ]),
  EffectPresetModel('Fuzz', [
  ]),
  EffectPresetModel('Overdrive', [
  ]),
  EffectPresetModel('Reverb', [
  ]),
];
