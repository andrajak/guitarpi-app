class Effect {

  static const String ECHO = 'Echo';
  static const String DELAY = 'Delay';
  static const String DISTORTION = 'Distortion';
  static const String FUZZ = 'Fuzz';
  static const String OVERDRIVE = 'Overdrive';
  static const String REVERB = 'Reverb';

  // non-linear
  static const String DISTORTION_LEVEL = 'Distortion_LEVEL';
  static const String FUZZ_LEVEL = 'Fuzz_LEVEL';
  static const String FUZZ_FUZZ = 'Fuzz_FUZZ';
  static const String OVERDRIVE_LEVEL = 'Overdrive_LEVEL';

  // echo
  static const String ECHO_LEVEL = 'Echo_LEVEL';
  static const String ECHO_TIME = 'Echo_TIME';

  // delay
  static const String DELAY_LEVEL = 'Delay_LEVEL';
  static const String DELAY_TIME = 'Delay_TIME';

  // reverb
  static const String REVERB_TIME = 'Reverb_TIME';
  static const String REVERB_WET = 'Reverb_WET';
  static const String REVERB_FB = 'Reverb_FB';

}